const express = require('express');
const path = require('path'); //path.join une nombres de direcotrios
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');//guardar datos de usuario a traves de sus sesion
const flash = require('connect-flash');

//Initializations
const app = express();
require('./database');

///////Settings
app.set('port', process.env.PORT || 3000);
app.set('views',path.join(__dirname, 'views') ); //Dandole la ruta de las views

//Configuración de las vistas:
app.engine('.hbs', exphbs({ //handdlebars es un motor de plantillas
  defaultLayout: 'main',//archivo principal de la plantilla de navegacion de las vistas
  layoutDir: path.join(app.get ('views'),'layouts'), //Directorio de los layaouts
  partialsDir: path.join(app.get ('views'),'partials'), //Directorio de los partials (pequeñas partes de codigo html que se pueden reutilizar en cualquier vista)
  extname: '.hbs' //etension de los archivos de vistas
}));

///////Aplicación de la configuración de las vistas
app.set ('view engine', '.hbs');

///////Middlewares
///////Cuando el ususario envía datos, se pueda entender
app.use(express.urlencoded({extended: false}));
app.use(methodOverride('_method')); //Para mandar methodos put y delete no solo get y post
app.use(session({
  secret: 'mysecretapp', //palabra secreta
  resave: true,
  saveUninitialized: true
}));
app.use(flash());

///////Global variables
app.use((req, res, next)=>{
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  next(); //para que no se quede cargando y guarde los datos
});

///////Routes
app.use(require('./routes/index'));
app.use(require('./routes/tandas'));
app.use(require('./routes/users'));

///////Static file-custom
app.use(express.static(path.join(__dirname, 'public')));

///////Server  is listening
app.listen(app.get ('port'), () => {
  console.log('Server on port: ', app.get('port'));
});
