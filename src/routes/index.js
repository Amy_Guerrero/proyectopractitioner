const express = require('express');
const router = express.Router();//Modulo para exportar rutas

router.get ('/', (req, res) => {
  //res.send('Index'); //La accion inicial cuando se ejecuta la aplicacion
  res.render('index');
});

router.get ('/about', (req, res) => {
  //res.send('about');
  res.render('about');

});

module.exports = router;
