const express = require('express');
const router = express.Router();//Modulo para exportar rutas

const User = require('../models/User');

router.get ('/users/signin', (req, res) => {
  //res.send('Ingresando a la app');
  res.render('users/signin');
});

router.get('/users/signup', (req, res) => {
  //res.send('Formulario de autenticacion');
  res.render('users/signup');
});

router.post('/users/signup', (req,res)=>{
  const {nombre, email, password, confirm_password} = req.body;
  console.log(req.body);
  const errors = [];
  if (nombre.length<=0 || email.length<=0 || password.length<=0 || confirm_password.length<=0) {
    errors.push({text: 'Por favor ingresa todos los datos'});
  }else{
    if (password != confirm_password) {
      errors.push({text: 'Las contraseñas no coinciden'});
    }
    if (password.length < 4) {
      errors.push({text: 'La contraseña debe ser mayor a 4 caracteres'});
    }
  }
  if (errors.length>0) {
    console.log("errors.length >0");
    res.render('users/signup', {errors, nombre, email, password, confirm_password});
  }else{
    const emailUser = User.findOne({email:email});
    if(emailUser){
      console.log("ya existe el eamil");
      req.flash('error_msg','El email ya etsa registrado')
      res.redirect('/users/signup');
    }//else{
      const newUser = new User({nombre, email, password});
      newUser.password = newUser.encryptPassword(function(err, password){
        if(err){
          console.log(err);
          res.send(400, 'Bad Request');
        }else{
          console.log("PAssword "+password);
           newUser.save(function(err, User) {//guardarlo en la bd, await para esperar a que temrine de guardar en la bd  y continue ejecutando
            if(err){
              console.log(err);
              res.send(400, 'Bad Request');
            }
            conosle.log("Success");
            req.flash('success_msg', 'Usuario registrado correctamente');
            res.redirect('/users/signin');
           })
         }
       });
    //}
  }
});
module.exports = router;
