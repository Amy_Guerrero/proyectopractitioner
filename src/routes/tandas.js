const express = require('express');
const router = express.Router();//Modulo para exportar rutas

const Tanda = require('../models/Tanda');

router.get('/tandas/add', (req, res) => {
  res.render('tandas/newTanda');
});

router.post('/tandas/new-tanda', (req, res) => {
  const {nombre, monto, periodoSemanas, numeros} = req.body;
  const errors = [];
  if(!nombre || !numeros || !periodoSemanas || !monto){
    errors.push({text: 'Por favor completa todos los campos'});
  }
  if(errors.length > 0){
    res.render('tandas/newTanda', {
      errors, nombre, monto, periodoSemanas, numeros
    });
  }else{
    const newTanda = new Tanda({nombre, monto, periodoSemanas, numeros});
    console.log(newTanda);
    newTanda.save(function(err, Tanda) {//guardarlo en la bd, await para esperar a que temrine de guardar en la bd  y continue ejecutando
      if(err){
        console.log(err);
        res.send(400, 'Bad Request');
      }
      req.flash('success_msg', 'Tanda agregada correctamente');
      res.redirect('/tandas');

      //console.log(err);
    })
  }
  //console.log(req.body);
});

router.get('/tandas', (req, res) => {
  Tanda.find().exec(function (err, tandas) {
    //.log(tandas);
    res.render('tandas/all-tandas', {tandas});
  });
});

router.get('/tandas/edit/:id', (req, res)=>{
  Tanda.findById(req.params.id).exec(function(err, tanda){
    console.log("Tandanombre: "+tanda.nombre);
    res.render('tandas/edit-tanda', {tanda});
  });
});

router.put('/tandas/edit-tanda/:id', (req,res) => {
  console.log("Id que llega:" + req.params.id);
  const {nombre, monto, periodoSemanas, numeros, fecha}=req.body;
  Tanda.findByIdAndUpdate(req.params.id, {nombre, monto, periodoSemanas, numeros, fecha}).exec(function(err,tanda){
    if (!err) {
      req.flash('success_msg', 'Tanda actualizada correctamente');
      res.redirect('/tandas');
    }
  })
});

router.delete('/tandas/delete/:id', (req, res) => {
  console.log(req.params.id);
  Tanda.findByIdAndDelete(req.params.id).exec(function(err, tanda){
    if (!err) {
      req.flash('success_msg', 'Tanda eliminada correctamente');
      res.redirect('/tandas');
    }
  })
});

module.exports = router;
