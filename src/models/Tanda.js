const mongoose = require('mongoose');
const {Schema} = mongoose;

//Propiedades de las tandas
const TandaSchema = new Schema({
  nombre: {
    type: String,
    required: true
  },
  monto: {
    type: Number,
    required: true,
  },
  periodoSemanas:{
    type: Number,
    required: true
  },
  numeros: {
    type: Number,
    required: true
  },
  participantes: {
    type: Object,
    default: [
        {
            "id1" : "5d44bd84ab4f01eb28ff98d5",
            "numeros" : 2.0
        },
        {
            "id2" : "5d4833f8e97cac36ddc0b916",
            "numeros" : 1.0
        },
        {
            "id3" : "5d4833f8e97cac36ddc0b917",
            "numeros" : 2.0
        }
    ]
  },
  fecha: {
      type: Date,
      default: Date.now
  }
});


/*
{
    "_id" : ObjectId("5dcca81a378823eadda6bcbb"),
    "nombre" : "Primera Tanda",
    "monto" : 100.0,
    "periodoSemanas" : 3.0,
    "numeros" : 5.0,
    "participantes" : [
        {
            "id1" : "5d44bd84ab4f01eb28ff98d5",
            "numeros" : 2.0
        },
        {
            "id2" : "5d4833f8e97cac36ddc0b916",
            "numeros" : 1.0
        },
        {
            "id3" : "5d4833f8e97cac36ddc0b917",
            "numeros" : 2.0
        }
    ],
    "fecha" : ISODate("2019-11-13T00:00:00.000+0000")
}*/
module.exports = mongoose.model('Tandas', TandaSchema);
